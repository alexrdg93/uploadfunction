# Function upload 

## How it works ?

To upload files, you need a form with multiple entries like this:

 
		<form enctype="multipart/form-data" id="uploadForm" method="post" action="upload.php">
			<div class="row">
				<div class="col s12 m8 l8 offset-l2">
					<div class="input-field col s6">
			          <input placeholder="Last name" id="lastName" name="lastName" type="text" class="validate">
			        </div>
			        <div class="input-field col s6">
			          <input placeholder="First name" id="firstName" name="firstName" type="text" class="validate">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col s12 l8 offset-l2">
			    	<div class="switch resize">
			    		<label>
			      			Crop the image :
			     			<input name="switch" type="checkbox" value="0">
			      			<span class="lever"></span>
			      			
			   			</label>
			  		</div>
		  		</div>
			</div>
			<div class="row">
			<div class="row inputMultiple">
				<div class="col s12 m8 l8 offset-l2">
		   			<div class="file-field input-field">
		      			<div class="btn">
		        		<span>File</span>
		        		<input type="file" name="ch_file_1">
		      		</div>
		      		<div class="file-path-wrapper">
		        		<input class="file-path validate" type="text">
		      		</div>
		    		</div>			  		
		  		</div>
			</div>
			<div class="row">
				<div class="col s12 m10 l8 offset-l5">
					<a class="waves-effect waves-effect btn" id="addInputFile"><i class="material-icons">add</i></a>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m8 l8 offset-l8">
					<button class="btn waves-effect waves-light" type="submit" id="sendUpload" name="upload">Submit
						<i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</form>

Then, create a JS File with:




```

$(document).ready(function(){
	$('.resize').on('change', function(){
		var value = $(this).find('input').attr('value');
		if(value == '0')
		{
			$(this).find('input').attr('value', '1');
			$('.hideResize').switchClass('hideResize', 'showResize');
		} else {
			$(this).find('input').attr('value', '0');
			$('.showResize').switchClass('showResize', 'hideResize');
		}		
	});
});

var i = "2";
$('#addInputFile').on('click', function(){
	
	$('.inputMultiple').append('<div class="col s12 m8 l8 offset-l2"><div class="file-field input-field"><div class="btn"><span>File</span><input type="file" name="ch_file_' + i +'"></div><div class="file-path-wrapper"><input class="file-path validate" type="text"></div></div></div>');
	i++;
});

```

AND INCLUDE METERIALIZE FOR A BETTER DESIGN, then follow steps


### 1. First


You need to define the way to upload your file :


To do this, it's require to create a folder on the server which will receive the file. Then, you wright in the construct :


Upload('way of the folder',);


### 2. Second


You need to define the different types of extension you need :


To do this, you just need to create an array with the following form ==> ['png', 'jpg', 'gif'] to push in the construct.


Do the same array to add extention of document like (.docx, .pdf, or other).


So we have a construct like this ==> Upload('way of the folder', ['ext_picture', 'ext_picture2'], ['ext_doc', 'ext_doc2'],);


### 3. Third


The last parameters to put in the construct are the width and height of the picture.


So, we can finish the construct and have a form like this :


Upload('way of folder', ['ext_pic', 'ext_pic2', ...], ['ext_doc', 'ext_doc2', ...], width, height);


Please, find the link on the following ref : http://rodriguez.etudiant-eemi.com/Perso/uploadfunction/