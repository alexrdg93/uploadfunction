<!DOCTYPE html>
<html>
<head>
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="assets/css/uploadFunction.css">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.js"></script>


<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta charset="utf-8">
<?php
require_once 'class/Upload.php';
require_once 'upload.php'
?>
</head>

<body>
<h1 style="text-align: center;">TEST ENVOIE DE FICHIER</h1><br>
	<div class="container">
		<form enctype="multipart/form-data" id="uploadForm" method="post" action="upload.php">
			<div class="row">
				<div class="col s12 m8 l8 offset-l2">
					<div class="input-field col s6">
			          <input placeholder="Last name" id="lastName" name="lastName" type="text" class="validate">
			        </div>
			        <div class="input-field col s6">
			          <input placeholder="First name" id="firstName" name="firstName" type="text" class="validate">
			        </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col s12 l8 offset-l2">
			    	<div class="switch resize">
			    		<label>
			      			Crop the image :
			     			<input name="switch" type="checkbox" value="0">
			      			<span class="lever"></span>
			      			
			   			</label>
			  		</div>
		  		</div>
		  		<!-- <div class="col s12 m8 l6 offset-l3 hideResize">
					<div class="input-field col s6">
			          <input id="resizeWidth" type="text" class="validate">
			          <label for="resizeWidth">Width</label>
			        </div>
			        <div class="input-field col s6">
			          <input id="resizeHeight" type="text" class="validate">
			          <label for="resizeHeight">Height</label>
			        </div>
		        </div> -->
			</div>
			<div class="row">
			<div class="row inputMultiple">
				<div class="col s12 m8 l8 offset-l2">
		   			<div class="file-field input-field">
		      			<div class="btn">
		        		<span>File</span>
		        		<input type="file" name="ch_file_1">
		      		</div>
		      		<div class="file-path-wrapper">
		        		<input class="file-path validate" type="text">
		      		</div>
		    		</div>			  		
		  		</div>
			</div>
			<div class="row">
				<div class="col s12 m10 l8 offset-l5">
					<a class="waves-effect waves-effect btn" id="addInputFile"><i class="material-icons">add</i></a>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m8 l8 offset-l8">
					<button class="btn waves-effect waves-light" type="submit" id="sendUpload" name="upload">Submit
						<i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="assets/js/materialize.js"></script>
	<script type="text/javascript" src="assets/js/uploadFunction.js"></script>
</body>
</html>