<?php

/**
 * Created by PhpStorm.
 * User: alexRdg
 * Date: 25/10/2016
 * Time: 09:28
 */
class Upload
{
    private $_extensionPicture = array();            // Extension of the picture
    private $_extension = array();                   // Extension of the file
    private $_folderDestination;                     // Folder of the uploaded file
                // ONLY FOR A PICTURE TYPE LIKE PNG,JPG,GIF
    private $_width;                                 // Width of the picture
    private $_height;                                // Height of the picture
    private $_newWidth;                              // Width after resize
    private $_newHeight;                             // Height after resize

    /**
     * @return mixed
     */
    public function getFolderDestination()
    {
        return $this->_folderDestination;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * @return mixed
     */
    public function getNewWidth()
    {
        return $this->_newWidth;
    }

    /**
     * @return mixed
     */
    public function getNewHeight()
    {
        return $this->_newHeight;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->_width = $width;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->_height = $height;
    }

    /**
     * @param mixed $newWidth
     */
    public function setNewWidth($newWidth)
    {
        $this->_newWidth = $newWidth;
    }

    /**
     * @param mixed $newHeight
     */
    public function setNewHeight($newHeight)
    {
        $this->_newHeight = $newHeight;
    }


    /*
     * Constructor
     */
    public function __construct($folderDestination,$extensionPicture,$extensionFile,$newWidth,$newHeight) {
        // Initialize parameters
        $this->_folderDestination = $folderDestination;
        $this->_extensionPicture = $extensionPicture;
        $this->_extension = $extensionFile;
        $this->_newWidth = $newWidth;
        $this->_newHeight = $newHeight;

        if (!empty($_FILES)) {
            foreach ($_FILES as $file) {
                $extUpload = strtolower(substr(strrchr($file['name'], '.'), 1));
                if (!in_array($extUpload,$extensionPicture) && (!in_array($extUpload, $extensionFile))) {
                    throw new InvalidArgumentException('File extension not allowed !');
                } if (in_array($extUpload,$extensionPicture)){
                    $this->uploadPicture($extUpload,$file);
                } if (in_array($extUpload,$extensionFile)) {
                    $this->uploadFile($extUpload,$file);
                }
            }
        }
    }

    /*
     * Method for picture type files
     */
    public function uploadPicture($extUpload,$file) {
            $url = $this->getFolderDestination().'/'.md5(uniqid(mt_rand())).".".$extUpload;
            if (move_uploaded_file($file['tmp_name'], $url)) {
                if (isset($_POST['switch'])) {
                    //  RESIZE JPG // JPEG
                    if ($extUpload == 'jpeg' || $extUpload == 'jpg') {
                        $image = imagecreatefromjpeg($url);
                        $this->setWidth(imagesx($image));
                        $this->setHeight(imagesy($image));
                        if ($this->getWidth() > $this->getHeight()) {
                            //FORMAT HORIZONTAL
                            $newWidth = $this->getNewWidth();
                            $newHeight = ($newWidth * $this->getHeight()) / $this->getWidth();

                        } else {
                            //FORMAT VERTICAL
                            $newHeight = $this->getNewHeight();
                            $newWidth = ($newHeight * $this->getWidth()) / $this->getHeight();
                        }
                        $thumb = imagecreatetruecolor($newWidth, $newHeight);
                        if (imagecopyresampled($thumb, $image, 0, 0, 0, 0, $this->getNewWidth(), $this->getNewHeight(), $this->getWidth(), $this->getHeight()))
                            echo "Resize OK<br>";
                        else
                            echo "Resize NOK<br>";

                        imagejpeg($thumb, $url);
                        imagedestroy($image);
                    }

                    //  RESIZE PNG
                    if ($extUpload == 'png') {
                        $image = imagecreatefrompng($url);
                        $this->setWidth(imagesx($image));
                        $this->setHeight(imagesy($image));

                        if ($this->getWidth()>$this->getHeight()) {
                            //FORMAT HORIZONTAL
                            $new_width = 500;
                            $new_height = ($new_width * $this->getHeight()) / $this->getWidth() ;

                        } else{
                            //FORMAT VERTICAL
                            $new_height = 375;
                            $new_width = ($new_height * $this->getWidth()) / $this->getHeight() ;
                        }
                        $thumb = imagecreatetruecolor($new_width, $new_height);

                        if (imagecopyresampled($thumb, $image, 0, 0, 0, 0, $new_width, $new_height, $this->getWidth(), $this->getHeight()))
                            echo "Resize OK<br>";

                        else
                            echo "Resize NOK<br>";

                        imagepng($thumb, $url);
                        imagedestroy($image);
                    }

                    // RESIZE GIF
                    if ($extUpload == 'gif') {
                        $image = imagecreatefromgif($url);
                        $this->setWidth(imagesx($image));
                        $this->setHeight(imagesy($image));
                        if ($this->getWidth()>$this->getHeight()) {
                            //FORMAT HORIZONTAL
                            $new_width = 500;
                            $new_height = ($new_width * $this->getHeight()) / $this->getWidth() ;

                        } else{
                            //FORMAT VERTICAL
                            $new_height = 375;
                            $new_width = ($new_height * $this->getWidth()) / $height ;
                        }
                        $thumb = imagecreatetruecolor($new_width, $new_height);
                        if (imagecopyresampled($thumb, $image, 0, 0, 0, 0, $new_width, $new_height, $this->getWidth(), $this->getHeight()))
                            echo "Resize OK<br>";
                        else
                            echo "Resize NOK<br>";

                        imagegif($thumb, $url);
                        imagedestroy($image);
                    }
                } else {
                    echo 'Nice !';
                }
            }
    }

    /*
     * Method for others files
     */
    public function uploadFile($extUpload,$file)
    {
            $url = $this->getFolderDestination() . '/' . md5(uniqid(mt_rand())) . "." . $extUpload;
            if (move_uploaded_file($file['tmp_name'], $url)) {
                echo 'cool';
            }
    }

}