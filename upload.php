<?php
/**
 * Created by PhpStorm.
 * User: alexRdg
 * Date: 25/10/2016
 * Time: 14:14
 */
require_once 'class/Upload.php';

$file = new Upload('uploads',['jpeg','jpg','png','gif'],['pdf','xls','doc','docx'],200,200);

?>
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/uploadFunction.css">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
    <script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <?php
    require_once 'class/Upload.php';
    require_once 'upload.php'
    ?>
</head>

<body>
<?php if(!empty($_FILES)){ ?>
    <h1>Félicitation, votre fichier s'est bien envoyé !</h1>
<?php
}
?>
</body>
</html>

